﻿using System;
using UnityEngine;


namespace Fx
{
    public class EffectsManager : MonoBehaviour
    {
        [SerializeField] private EffectsContainer effectsContainer = default;

        public static EffectsManager Instance { get; private set; }

        public EffectsContainer EffectsContainer =>
            effectsContainer;


        public void Awake()
        {
            Instance = this;
        }

        public bool TryPlayOnce(string fxName, out Effect playedEffect)
        {
            bool result = TryPlayOnce(FindEffect(fxName), out Effect playedEffect1);
            playedEffect = playedEffect1;

            return result;
        }


        public bool TryPlayOnce(Effect effect, out Effect playedEffect)
        {
            playedEffect = default;

            if (effect == null)
            {
                return false;
            }

            playedEffect = Instantiate(effect, transform);
            var module = playedEffect.MainParticle.main;
            module.stopAction = ParticleSystemStopAction.Disable; // for pool
            playedEffect.Play();

            return playedEffect != null;
        }

        public Effect FindEffect(string fxName)
        {
            Effect effect = Array.Find(effectsContainer.Effects, e => e.name == fxName);

            if (effect == null)
            {
                Debug.Log($"No {nameof(Effect)} was found by name {fxName}");
            }

            return effect;
        }
    }
}
