﻿using UnityEngine;


namespace Fx
{
    public class Effect : MonoBehaviour
    {
        [SerializeField] private ParticleSystem mainParticle = default;

        public ParticleSystem MainParticle =>
            mainParticle;


        public void Play()
        {
            mainParticle.Play(true);
        }
    }
}
