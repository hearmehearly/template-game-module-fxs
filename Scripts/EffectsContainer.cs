﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;


namespace Fx
{
    public class EffectsContainer : ScriptableObject
    {
        private const string RESOURCES_DIRECTORY = "Assets/Effects";


        [SerializeField] private Effect[] effects = default;


        public Effect[] Effects =>
            effects;

#if UNITY_EDITOR

        private List<Effect> LoadEffects()
        {
            List<Effect> result = new List<Effect>();

            if (Directory.Exists(RESOURCES_DIRECTORY))
            {
                PerformActionOnFilesRecursively(RESOURCES_DIRECTORY, fileName =>
                {
                    Effect loadedHandler = AssetDatabase.LoadAssetAtPath<Effect>(fileName);

                    if (loadedHandler != null)
                    {
                        result.Add(loadedHandler);
                    }
                });
            }

            return result;
        }


        private void PerformActionOnFilesRecursively(string rootDir, Action<string> action)
        {
            foreach (string dir in Directory.GetDirectories(rootDir))
            {
                PerformActionOnFilesRecursively(dir, action);
            }

            foreach (string file in Directory.GetFiles(rootDir))
            {
                action(file);
            }
        }

        private void OnValidate()
        {
            effects = LoadEffects().ToArray();
        }
#endif
    }
}
